import Account.LoginFacebookPage;
import Account.LogoutFacebookPage;
import Utils.MyWaits;
import Utils.DataProv;
import Other.CookeisPage;
import Other.SelectLanguagePage;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class MyTest {
    WebDriver driver;
    private MyWaits wait;
    @BeforeClass
    void setup() {
        //System.setProperty("webdriver.chrome.driver", "C:\\Users\\Home\\Downloads\\chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\it-school\\Desktop\\setup\\chromedriver.exe");
        driver = (WebDriver) new ChromeDriver();
        // driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.get("https://www.templatemonster.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        wait = new MyWaits(driver);
    }
    @Test
    void testSearchCookies (){
        CookeisPage cookeisPage = PageFactory.initElements(driver, CookeisPage.class);
        wait.waitCookies("aff");
        Assert.assertEquals(cookeisPage.getCookies("aff"),"TM");
    }

    @Test(dataProvider = "data-provider", dataProviderClass = DataProv.class)
    public void testSelectLanguage (String line) {
         wait.waitClickHeart();
        SelectLanguagePage selectLanguagePage = PageFactory.initElements(driver, SelectLanguagePage.class);
       selectLanguagePage.selectLanguage(line);
        Assert.assertEquals(driver.getCurrentUrl(),"https://www.templatemonster.com/"+line.toLowerCase()+"/");
    }
    @Test
    public void logIn() {
        LoginFacebookPage loginFacebookPage = PageFactory.initElements(driver, LoginFacebookPage.class);
        loginFacebookPage.loginFacebook("litstest@i.ua","Linux-32");
        Assert.assertTrue(loginFacebookPage.isLogged());
    }
    @AfterClass
    public void tearDown() {
        LogoutFacebookPage logoutFacebookPage = PageFactory.initElements(driver, LogoutFacebookPage.class);
        logoutFacebookPage.logoutFacebook();
        driver.quit();
    }
}
