import Other.OrderProcPage;
import Account.RegistrationPage;
import Utils.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class RegistrationTest {
  static WebDriver driver;
  private static MyWaits wait;

  @BeforeClass
  void setup() throws MalformedURLException {
  //  DesiredCapabilities capability = DesiredCapabilities.chrome();
//    ChromeOptions options = new ChromeOptions();
//    options.addArguments("--start-maximized");
//    capability.setCapability(ChromeOptions.CAPABILITY, options);
    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\Yuliya\\Desktop\\setup\\chromedriver.exe");
   System.setProperty("webdriver.chrome.driver", "C:\\Users\\Home\\Desktop\\sel\\chromedriver.exe");
    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\it-school\\Desktop\\setup\\chromedriver.exe");
 driver = (WebDriver) new ChromeDriver();
    //DesiredCapabilities capability = DesiredCapabilities.firefox();
  //driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);
   // driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    wait = new MyWaits(driver);
    Scrin scrin=new Scrin();
    //https://gyazo.com/7127e56a8535b15e9b87a24b418abf84
    //https://gyazo.com/fdca98c3dd730b522cc630e0610fcbda
    }
   //@Test(dataProvider = "users", dataProviderClass = DataProv.class)
@Test
    public void registrationTest(){
    OrderProcPage orderProcPage = PageFactory.initElements(driver, OrderProcPage.class);
    orderProcPage.chekout();
    RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
    User user=new User("src/test/resources/UkrainianUser.properties");
    registrationPage.registerAccount(user);
    wait.waitPayment();
    Assert.assertTrue(registrationPage.isPaymentMethod());
          }

  public void screenshotFailure(String filename)  {
    TakesScreenshot scrShot =((TakesScreenshot)driver);
    File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
    File targetFile=new File("./Screenshots/Failure/"  + filename +".jpg");
    try {
      FileUtils.copyFile(SrcFile,targetFile);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
//  @AfterMethod
//  void deleteCookies(){
//    driver.manage().deleteAllCookies();
//  }

  @AfterTest
  public void tearDown() {
    screenshotFailure("test.png");
    driver.quit();
  }
  }

