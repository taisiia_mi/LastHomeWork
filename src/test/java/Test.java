import Utils.MyWaits;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Home on 23.10.2017.
 */
public class Test {
 @org.testng.annotations.Test
  void test () throws MalformedURLException {

//    DesiredCapabilities capabilities = new DesiredCapabilities();
//
//// ... but only if it supports javascript
//    //capabilities.setJavascriptEnabled(true);
//
//// Get a handle to the driver. This will throw an exception
//// if a matching driver cannot be located
//    WebDriver driver = new RemoteWebDriver(capabilities);
//
//// Query the driver to find out more information
//    Capabilities actualCapabilities = ((RemoteWebDriver) driver).getCapabilities();

   DesiredCapabilities capability = DesiredCapabilities.chrome();
   WebDriver driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);
   //  driver.manage().window().maximize();
   driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
   driver.get("https://www.templatemonster.com/ua/html-templates/62319.html");

// And now use it
    driver.get("http://www.google.com");

  }

}
