package Account;
import Utils.Click;
import Utils.MyWaits;
import Utils.User;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
public class RegistrationPage {
  private final WebDriver driver;
  public RegistrationPage(WebDriver driver) {
    this.driver = driver;
  }
  @FindBy(id = "billinginfo3-form-fullname")
  WebElement namefield;

  @FindBy(xpath = ".//*[@id='billinginfo3_form_countryiso2_chosen']/a")
  WebElement countryfield;

  @FindBy(xpath = ".//*[@placeholder='Start enter country']")
  WebElement countryinput;

  @FindBy(xpath = ".//*[@id='billinginfo3_form_phone_code_chosen']/a/div")
  WebElement dropdownphone;

  @FindBy(xpath =".//*[@id='billinginfo3_form_phone_code_chosen']/div/div/input")
  WebElement phoneinput;

  @FindBy(id = "billinginfo3-form-phone")
  WebElement phonesecondfield;

  @FindBy(id = "billinginfo3-form-postalcode")
  WebElement zipcodefield;

  @FindBy(xpath = ".//*[@id='billinginfo3_form_stateiso2_chosen']/a/div")
  WebElement statefield;

  @FindBy(id = "billinginfo3-form-cityname")
  WebElement cityfield;

  @FindBy(xpath=".//*[contains(@class,'signin-switch-account')]")
  WebElement swithaccount;

  @FindBy(xpath = ".//*[contains(@id,'signin3-form-email')]")
  WebElement emailfield;

  @FindBy(id ="signin3-new-customer")
  WebElement continuebuton;

  @FindBy(id ="billinginfo3-form-submit")
  WebElement submitbutton;

  @FindBy(className ="signed-title")
  WebElement panel;

  public void registerAccount(User user) {
    Click click=new Click(driver);
    MyWaits myWaits = new MyWaits(driver);

    if (panel.isDisplayed()) {
      panel.click();
      swithaccount.click();
    }
    myWaits.waitEmail();
    emailfield.sendKeys(user.generateEmail());
       click.methodClick(continuebuton);
        myWaits.waitregForm();
    click.methodClick(namefield);
    namefield.sendKeys(user.getName());
    click.methodClick(namefield);
    click.methodClick(countryfield);
    countryinput.sendKeys(user.getCountry());
    click.methodClick(countryinput);
    click.methodClick(dropdownphone);
    phoneinput.sendKeys(user.getPhone1());
    click.methodClick(phoneinput);
    phoneinput.sendKeys(Keys.ENTER);
    phonesecondfield.sendKeys(user.getPhone2());
    click.methodClick(phonesecondfield);
    zipcodefield.sendKeys(user.getZipcode());
    click.methodClick(zipcodefield);
    zipcodefield.sendKeys(Keys.TAB);
    if (countryfield.getText().equals("United States")){
    statefield.sendKeys(user.getState());
    click.methodClick(statefield);
      }
      click.methodClick(cityfield);
      cityfield.sendKeys(user.getCity());
    click.methodClick(cityfield);
    click.methodClick(submitbutton);
    }
  public boolean isPaymentMethod(){
    return driver.findElement(By.xpath(".//*[@id='payment-methods-container']/div[1]")).isDisplayed();
  }
}
