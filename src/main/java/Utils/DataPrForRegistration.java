package Utils;
import org.testng.annotations.DataProvider;

/**
 * Created by Home on 14.10.2017.
 */

public class DataPrForRegistration {
  @DataProvider(name = "users")
  public Object[][] users() {
    return new String[][]{
            {"src/test/resources/UkrainianUser.properties"},
            {"src/test/resources/USUser.properties"},

    };
  }
}
