package Utils;
import java.io.*;
import java.util.Random;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Properties;
public class User {
    private  String name;
    private String country;
    private String city;
    private String phone1;
    private String phone2;
    private String zipcode;
    private String state;
    Properties property = new Properties();
    public static FileInputStream fileInputStream;
    public User(String path) {
               try {
            property.load(new InputStreamReader(new FileInputStream(path), "UTF-8"));
            name = property.getProperty("name");
            country = property.getProperty("country");
            city = property.getProperty("city");
            phone1 = property.getProperty("phonenumber1");
            phone2 = property.getProperty("phonenumber2");
            zipcode = property.getProperty("zipcode");
            state = property.getProperty("state");
               } catch (IOException e) {
                   System.err.println("ОШИБКА: Файл свойств отсуствует!");
               } finally {
                   if (fileInputStream != null)
                       try {
                           fileInputStream.close();
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
        }
    }
    public String getName() {
        return name;
    }

    public String getCountry() {
      return country;
    }

    public String getCity() {
        return city;
    }

    public String getPhone1() {
        return phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public String getZipcode() {
        return zipcode;
    }

    public String getState() {
        return state;
    }

    public String generateEmail() {
        Random random = new Random();
        String symbols = "1234567890abcdefghijklmnopqrstuvwxyz";
        int pos = random.nextInt(symbols.length());
        StringBuilder randString = new StringBuilder();
        for (int i = 0; i < 8; i++)
            randString.append(symbols.charAt((int) (Math.random() * symbols.length())));
        String email=randString+"@gmail.com";
        return email;
    }

}