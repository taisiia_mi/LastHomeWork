package Utils;
import org.testng.annotations.DataProvider;

/**
 * Created by it-school on 11.10.2017.
 */
public class DataProv {
    @DataProvider(name = "data-provider")
    public Object[][] language() {
        return new Object[][]{
                {"RU"},
                {"PL"},
                {"IT"},
        };
    }

    @DataProvider(name = "users")
    public Object[][] users() {
        return new String[][]{
                {"src/test/resources/UkrainianUser.properties"},
                {"src/test/resources/USUser.properties"},

        };
    }
}
